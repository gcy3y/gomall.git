kitexcall -t protobuf -m ProductCatalogService/ListProducts -f listProduct.json -e 10.0.2.15:8882 -p ../../idl/product.proto
kitexcall -t protobuf -m ProductCatalogService/GetProduct -f getProduct.json -e 10.0.2.15:8882 -p ../../idl/product.proto
kitexcall -t protobuf -m ProductCatalogService/SearchProducts -f searchProduct.json -e 10.0.2.15:8882 -p ../../idl/product.proto
