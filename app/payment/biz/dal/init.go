package dal

import (
	"gitee.com/gcy3y/gomall/app/payment/biz/dal/mysql"
	"gitee.com/gcy3y/gomall/app/payment/biz/dal/redis"
)

func Init() {
	redis.Init()
	mysql.Init()
}
