package dal

import (
	"gitee.com/gcy3y/gomall/app/product/biz/dal/mysql"
	"gitee.com/gcy3y/gomall/app/product/biz/dal/redis"
)

func Init() {
	redis.Init()
	mysql.Init()
}
