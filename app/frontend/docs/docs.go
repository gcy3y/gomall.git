// Package docs Code generated by swaggo/swag. DO NOT EDIT
package docs

import "github.com/swaggo/swag"

const docTemplate = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "contact": {},
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/": {
            "get": {
                "responses": {}
            }
        },
        "/about": {
            "post": {
                "responses": {}
            }
        },
        "/auth/login": {
            "post": {
                "responses": {}
            }
        },
        "/auth/logout": {
            "post": {
                "responses": {}
            }
        },
        "/auth/register": {
            "post": {
                "responses": {}
            }
        },
        "/cart": {
            "get": {
                "responses": {}
            },
            "post": {
                "responses": {}
            }
        },
        "/category/:category": {
            "get": {
                "responses": {}
            }
        },
        "/checkout": {
            "get": {
                "responses": {}
            }
        },
        "/checkout/result": {
            "get": {
                "responses": {}
            }
        },
        "/checkout/waiting": {
            "post": {
                "responses": {}
            }
        },
        "/order": {
            "get": {
                "responses": {}
            }
        },
        "/product": {
            "get": {
                "responses": {}
            }
        },
        "/search": {
            "get": {
                "responses": {}
            }
        }
    }
}`

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = &swag.Spec{
	Version:          "",
	Host:             "",
	BasePath:         "",
	Schemes:          []string{},
	Title:            "",
	Description:      "",
	InfoInstanceName: "swagger",
	SwaggerTemplate:  docTemplate,
	LeftDelim:        "{{",
	RightDelim:       "}}",
}

func init() {
	swag.Register(SwaggerInfo.InstanceName(), SwaggerInfo)
}
