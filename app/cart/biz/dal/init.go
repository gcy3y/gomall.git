package dal

import (
	"gitee.com/gcy3y/gomall/app/cart/biz/dal/mysql"
	"gitee.com/gcy3y/gomall/app/cart/biz/dal/redis"
)

func Init() {
	redis.Init()
	mysql.Init()
}
