package dal

import (
	"gitee.com/gcy3y/gomall/app/user/biz/dal/mysql"
	"gitee.com/gcy3y/gomall/app/user/biz/dal/redis"
)

func Init() {
	redis.Init()
	mysql.Init()
}
