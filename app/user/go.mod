module gitee.com/gcy3y/gomall/app/user

go 1.22.2

replace (
	github.com/apache/thrift => github.com/apache/thrift v0.13.0
	gitee.com/gcy3y/gomall/common => ../../common
	gitee.com/gcy3y/gomall/rpc_gen => ../../rpc_gen
)

require github.com/golang/protobuf v1.5.4 // indirect
