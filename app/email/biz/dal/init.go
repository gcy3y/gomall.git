package dal

import (
	"gitee.com/gcy3y/gomall/app/email/biz/dal/mysql"
	"gitee.com/gcy3y/gomall/app/email/biz/dal/redis"
)

func Init() {
	redis.Init()
	mysql.Init()
}
