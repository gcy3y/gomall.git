module gitee.com/gcy3y/gomall/app/checkout

go 1.22.2

replace (
	gitee.com/gcy3y/gomall/common => ../../common
	gitee.com/gcy3y/gomall/rpc_gen => ../../rpc_gen
	github.com/apache/thrift => github.com/apache/thrift v0.13.0
)

require github.com/nats-io/nats.go v1.37.0

require github.com/go-logr/logr v0.1.0 // indirect

require (
	github.com/golang/protobuf v1.5.4 // indirect
	github.com/klauspost/compress v1.17.2 // indirect
	github.com/nats-io/nkeys v0.4.7 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/u2takey/go-utils v0.3.1
	golang.org/x/crypto v0.18.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
)
