package dal

import (
	"gitee.com/gcy3y/gomall/app/checkout/biz/dal/mysql"
	"gitee.com/gcy3y/gomall/app/checkout/biz/dal/redis"
)

func Init() {
	redis.Init()
	mysql.Init()
}
