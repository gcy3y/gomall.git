#!/bin/bash

svcName=${1}

cd app/${svcName}
cwgo client -I ../../idl --type RPC --service ${svcName} --module gitee.com/gcy3y/gomall/app/${svcName} --idl ../../idl/${svcName}.proto
cwgo server -I ../../idl --type RPC --service ${svcName} --module gitee.com/gcy3y/gomall/app/${svcName} --idl ../../idl/${svcName}.proto
