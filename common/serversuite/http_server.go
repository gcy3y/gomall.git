package serversuite

import (
	"github.com/cloudwego/hertz/pkg/app/server"
	"github.com/cloudwego/hertz/pkg/app/server/registry"
	"github.com/cloudwego/hertz/pkg/common/config"
	"github.com/cloudwego/hertz/pkg/common/hlog"
	"github.com/cloudwego/hertz/pkg/common/utils"
	consulapi "github.com/hashicorp/consul/api"
	"github.com/hertz-contrib/registry/consul"
)

type CommonHttpServerSuite struct {
	CurrentServiceName string
	RegistryAddr       string
}

func (s CommonHttpServerSuite) Options() []config.Option {
	opts := []config.Option{}
	conConfig := consulapi.DefaultConfig()
	conConfig.Address = s.RegistryAddr
	consulClient, err := consulapi.NewClient(conConfig)
	if err != nil {
		hlog.Fatal(err)
	}
	// build a consul register with the consul client
	r := consul.NewConsulRegister(consulClient)

	// run Hertz with the consul register
	cR := server.WithRegistry(r, &registry.Info{
		ServiceName: s.CurrentServiceName,
		Addr:        utils.NewNetAddr("tcp", "10.0.2.15:8080"),
		Weight:      10,
		Tags:        nil,
	})
	opts = append(opts, cR)

	return opts
}
